/*
* Copyright (c) 2017 Andrew Luchuk
*
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, merge, 
* publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
* to whom the Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
* BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
* DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package speratus.jcapture;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class FrameWriterThread implements Runnable {

	private volatile boolean running = true;
	private volatile ArrayList<BufferedImage> images = new ArrayList<BufferedImage>();
	private File outputLoc;
	private String format = "png";
	private long icounter = 0;
	
	public FrameWriterThread(File loc) {
		this.outputLoc = loc;
	}
	
	@Override
	public void run() {
		while (running || !images.isEmpty()) {
			while (!images.isEmpty()) {
				BufferedImage img = images.get(0);
				String name = Long.toString(icounter);
				try {
					ImageIO.write(img, format, new File(outputLoc.toString() + File.separator + name + "." + format));
					icounter++;
					images.remove(0);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void addImage(BufferedImage i) {
		images.add(i);
	}
	
	public void stop() {
		running = false;
	}

}
