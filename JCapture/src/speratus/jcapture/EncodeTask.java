/*
* Copyright (c) 2017 Andrew Luchuk
*
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, merge, 
* publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
* to whom the Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
* BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
* DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package speratus.jcapture;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.SwingWorker;

import org.jcodec.api.SequenceEncoder8Bit;
import org.jcodec.scale.AWTUtil;

public class EncodeTask extends SwingWorker<Void, Void> {

	private File imgDir;
	private SequenceEncoder8Bit encoder;
	
	public EncodeTask(File imgDir, File outFile, int fps) throws IOException {
		this.imgDir = imgDir;
		encoder = SequenceEncoder8Bit.createSequenceEncoder8Bit(outFile, fps);
	}
	
	@Override
	protected Void doInBackground() throws Exception {
		int progress = 0;
		setProgress(progress);
		int fileNum = imgDir.listFiles().length;
		String path = imgDir.toString() + File.separator;
		
		for (; progress < fileNum; progress++) {
			File img = new File(path + progress + ".png");
			BufferedImage bi = ImageIO.read(img);
			
			encoder.encodeNativeFrame(AWTUtil.fromBufferedImageRGB8Bit(bi));
			setProgress(progress);
		}
		
		return null;
	}

	@Override
	public void done() {
		try {
			encoder.finish();
			imgDir.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
