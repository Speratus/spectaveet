/*
* Copyright (c) 2017 Andrew Luchuk
*
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, merge, 
* publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
* to whom the Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
* BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
* DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package speratus.jcapture;

import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JProgressBar;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class ProgressDialog extends JDialog implements PropertyChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private final String LABEL_TEXT = "progress: ";
	private JProgressBar progressBar;
	private JLabel label;
	private int bound = 0;
	private EncodeTask task = null;

	/**
	 * Launch the application.
	 *
	public static void main(String[] args) {
		try {
			ProgressDialog dialog = new ProgressDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/

	/**
	 * Create the dialog.
	 */
	public ProgressDialog() {
		setTitle("Progress Completion");
		setBounds(100, 100, 450, 107);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.SOUTH);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			progressBar = new JProgressBar();
			contentPanel.add(progressBar);
		}
		{
			label = new JLabel("progress: 0/0");
			label.setHorizontalAlignment(SwingConstants.CENTER);
			getContentPane().add(label, BorderLayout.CENTER);
		}
	}
	
	public void startTask(File imgDir, File outFile, int fps) {
		try {
			bound = imgDir.listFiles().length;
			
			label.setText(LABEL_TEXT + "0/" + bound);
			progressBar.setMinimum(0);
			progressBar.setMaximum(bound);
			
			this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			this.setVisible(true);                                  
			
			task = new EncodeTask(imgDir, outFile, fps);
			task.addPropertyChangeListener(this);
			task.execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if ("progress".equals(evt.getPropertyName())) {
			int progress = (Integer) evt.getNewValue();
			progressBar.setValue(progress);
			label.setText(LABEL_TEXT + progress +"/" + bound);
			
			if (progress == bound && task.isDone()) {
				this.dispose();
			}
		}
	}

}
