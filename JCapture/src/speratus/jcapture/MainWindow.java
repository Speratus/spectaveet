/*
* Copyright (c) 2017 Andrew Luchuk
*
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, merge, 
* publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
* to whom the Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
* BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
* DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package speratus.jcapture;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.awt.event.ActionEvent;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class MainWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private FrameWriterThread writer;
	private CaptureTask capturer;
	private File videoStorageLoc;
	private int fps = 30;
	private File currentImgLoc;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	
	/**
	 * Create the frame.
	 */
	public MainWindow() {
		videoStorageLoc = new File(System.getProperty("user.dir") + File.separator + "JCapture");
		if (!videoStorageLoc.exists())
			videoStorageLoc.mkdirs();
		
		setTitle("JCapture");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 261, 118);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnOptions = new JMenu("options");
		menuBar.add(mnOptions);
		
		JCheckBoxMenuItem chckbxmntmMinimizeOnStart = new JCheckBoxMenuItem("minimize on start");
		mnOptions.add(chckbxmntmMinimizeOnStart);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JButton startRec = new JButton("Start Recording");
		startRec.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				startRecording();
			}
		});
		contentPane.setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.UNRELATED_GAP_COLSPEC,
				ColumnSpec.decode("107px"),
				FormSpecs.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("105px"),},
			new RowSpec[] {
				FormSpecs.UNRELATED_GAP_ROWSPEC,
				RowSpec.decode("23px"),}));
		contentPane.add(startRec, "2, 2, left, top");
		
		JButton stopRec = new JButton("Stop Recording");
		stopRec.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				stopRecording();
			}
		});
		contentPane.add(stopRec, "4, 2, left, top");
	}
	
	private long calculatePausePeriod(int fps) {
		return 1000L/fps;
	}
	
	public void initThreads() {
		
	}
	
	
	public void startRecording() {
		Date d = new Date();
		SimpleDateFormat f = new SimpleDateFormat("yyyy-mm-dd-hh-ss");
		
		String name = f.format(d);
		currentImgLoc = new File(videoStorageLoc + File.separator + name);
		
		writer = new FrameWriterThread(currentImgLoc);
		Thread t = new Thread(writer);
		t.start();
		
		capturer = new CaptureTask(writer);
		
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(capturer, 0, calculatePausePeriod(fps));
	}
	
	private void displaySaveDialog() {
		JFileChooser chooser = new JFileChooser();

		int returnVal = chooser.showSaveDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File output = chooser.getSelectedFile();
			ProgressDialog dialog = new ProgressDialog();
			dialog.startTask(currentImgLoc, output, fps);
		} else {
			int approve = JOptionPane.showConfirmDialog(this, "Are you sure you want to discard the video?",
					"Save Video", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
			
			if (approve == JOptionPane.YES_OPTION) {
				currentImgLoc.delete();
			} else {
				displaySaveDialog();
			}
		}
	}
	
	public void stopRecording() {
		capturer.cancel();
		writer.stop();
		
		displaySaveDialog();
	}
	
}
