# README #

This software is licensed under the MIT License.

## LICENSE ##

Copyright (c) 2017 Andrew Luchuk

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


### What is this repository for? ###

This project came about as I was frustrated with the lack of free screen capture software out there that actually worked. 
I had previously written a tiny proof of concept program that could capture screenshots at regular intervals, so I decided to polish it up
and turn it into a full-blown Screen capture program. At the moment, it is too incomplete to run, but soon I hope it will be complete enough
to be useful.

### Setup ###

1. Setup eclispe with a clone of this repository.
2. Clone the library [JCodec](https://github.com/jcodec/jcodec) from Github.
3. In the eclipse buildpath, create new source links to the main jcodec source folder and also the jcodec-se source folder.

After that, you should be good to go.

### Goals ###

At the moment, the primary goal of this repository is to produce a program which simply records the video on the screen.

At some later point, I will add audio support, but at the moment, I'm just focusing on getting a working video program.